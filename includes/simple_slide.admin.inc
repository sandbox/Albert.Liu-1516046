<?php
/**
 * @file simple_slide.admin.inc
 * This is Config form for Simple Slide settings.
 */

/**
 * General configuration form for controlling the Simple Slide behaviour.
 */
function simple_slide_admin_settings() {
  $description = t("Setting for Sliders.");
  $easing = array(
    'swing' => t('swing'),
    'easeInQuad' => t('easeInQuad'),
    'easeOutQuad' => t('easeOutQuad'),
    'easeInOutQuad' => t('easeInOutQuad'),
    'easeInCubic' => t('easeInCubic'),
    'easeOutCubic' => t('easeOutCubic'),
    'easeInOutCubic' => t('easeInOutCubic'),
    'easeInQuart' => t('easeInQuart'),
    'easeOutQuart' => t('easeOutQuart'),
    'easeInOutQuart' => t('easeInOutQuart'),
    'easeInQuint' => t('easeInQuint'),
    'easeOutQuint' => t('easeOutQuint'),
    'easeInOutQuint' => t('easeInOutQuint'),
    'easeInSine' => t('easeInSine'),
    'easeOutSine' => t('easeOutSine'),
    'easeInOutSine' => t('easeInOutSine'),
    'easeInExpo' => t('easeInExpo'),
    'easeOutExpo' => t('easeOutExpo'),
    'easeInOutExpo' => t('easeInOutExpo'),
    'easeInCirc' => t('easeInCirc'),
    'easeOutCirc' => t('easeOutCirc'),
    'easeInOutCirc' => t('easeInOutCirc'),
    'easeInElastic' => t('easeInElastic'),
    'easeOutElastic' => t('easeOutElastic'),
    'easeInOutElastic' => t('easeInOutElastic'),
    'easeInBack' => t('easeInBack'),
    'easeOutBack' => t('easeOutBack'),
    'easeInOutBack' => t('easeInOutBack'),
    'easeInBounce' => t('easeInBounce'),
    'easeOutBounce' => t('easeOutBounce'),
    'easeInOutBounce' => t('easeInOutBounce'),
  );
  $form['simple_slide_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => variable_get('simple_slide_width', '700'),
    '#size' => 10,
    '#required' => TRUE,
    '#description' => t('Display for Simple Slide Width. (Default: 700)'),
  );
  $form['simple_slide_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => variable_get('simple_slide_height', '300'),
    '#size' => 10,
    '#required' => TRUE,
    '#description' => t('Display for Simple Slide Height. (Default: 300)'),
  );
  $form['simple_slide_readMore'] = array(
    '#type' => 'checkbox',
    '#title' => t('Read More'),
    '#default_value' => variable_get('simple_slide_readMore', 1),
    '#description' => t('Provide a Read More Link in Simple Slide content. (Default: checked)'),
  );
  $form['simple_slide_imageLink'] = array(
    '#type' => 'checkbox',
    '#title' => t('Image Link'),
    '#default_value' => variable_get('simple_slide_imageLink', 1),
    '#description' => t('Image as a Link < a > image < /a >. (Default: checked)'),
  );
  $form['simple_slide_effect'] = array(
    '#type' => 'select',
    '#title' => t('Slide Effect'),
    '#default_value' => variable_get('simple_slide_effect', 'slide'),
    '#options' => array('slide' => t('Slide'), 'fade' => t('Fade')),
    '#required' => TRUE,
    '#description' => t('Chage Simple Slide Effect. (Default: Slide)'),
  );
  $form['simple_slide_easing'] = array(
    '#type' => 'select',
    '#title' => t('Slide Easing'),
    '#default_value' => variable_get('simple_slide_easing', 'easeInOutExpo'),
    '#options' => $easing,
    '#required' => TRUE,
    '#description' => t('Display for Simple Slide easing. (Default: easeInOutExpo)'),
  );
  $form['simple_slide_preload'] = array(
    '#type' => 'checkbox',
    '#title' => t('Preload'),
    '#default_value' => variable_get('simple_slide_preload', 1),
    '#description' => t('When this image loading, it will display a preload gif to wait image loading. (Default: checked)'),
  );
  $form['simple_slide_playControl'] = array(
    '#type' => 'checkbox',
    '#title' => t('Play Controler'),
    '#default_value' => variable_get('simple_slide_playControl', 1),
    '#description' => t('Provide a Play Controler. (Default: checked)'),
  );
  $form['simple_slide_generatePagination'] = array(
    '#type' => 'checkbox',
    '#title' => t('Navigator'),
    '#default_value' => variable_get('simple_slide_generatePagination', 1),
    '#description' => t('Display navigator. (Default: checked)'),
  );
  $form['simple_slide_randomize'] = array(
    '#type' => 'checkbox',
    '#title' => t('Randomize'),
    '#default_value' => variable_get('simple_slide_randomize', 0),
    '#description' => t('Randomize all of Simple Slide image. (Default: uncheck)'),
  );
  $form['simple_slide_effectSpeed'] = array(
    '#type' => 'textfield',
    '#title' => t('Animation Speed'),
    '#default_value' => variable_get('simple_slide_effectSpeed', '350'),
    '#size' => 10,
    '#required' => TRUE,
    '#description' => t('Animation of Slides speed. (Default: 350)'),
  );
  $form['simple_slide_play'] = array(
    '#type' => 'textfield',
    '#title' => t('Sliding Speed'),
    '#default_value' => variable_get('simple_slide_play', '5000'),
    '#size' => 10,
    '#required' => TRUE,
    '#description' => t('Each Slides Interval. (Default: 5000)'),
  );
  $form['simple_slide_pause'] = array(
    '#type' => 'textfield',
    '#title' => t('Hover Pause Timer'),
    '#default_value' => variable_get('simple_slide_pause', '3500'),
    '#size' => 10,
    '#required' => TRUE,
    '#description' => t('When the mouse move in or out, it will set the Pause Timer. If set 0, it will never go. (Default: 3500)'),
  );
  $form['simple_slide_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Css Path'),
    '#default_value' => variable_get('simple_slide_css', ''),
    '#size' => 40,
    '#maxlength' => 255,
    '#description' => t('Leave blank to use default style.<br />Enter the path to the CSS file. (Example: !example)',
      array(
        '!example' => '<code>"css/custom.css"</code>',
      )
    ) .
    '<br />' .
    t('Available placeholders: !h – host name (!host). !t – path to theme (!theme).',
      array(
        '!h' => '<br /><code>%h</code>',
        '!t' => '<br /><code>%t</code>',
        '!host' => '<code>' . base_path() . '</code>',
        '!theme' => '<code>' . base_path() . drupal_get_path('theme', variable_get('theme_default', NULL)) . '/</code>',
      )
    ) .
    '<br />' .
    t('<strong>Images will be displayed when you add slide width and height.</strong>!style',
      array(
        '!style' => '<br /><code>#simple-slide .slides_container { height: 300px; width: 700px; }</code>',
      )
    ),
  );
  $form['#submit'][] = 'simple_slide_admin_settings_submit';
  return system_settings_form($form);
}

/**
 * Process simple slide effects and programmatically create a css file
 */
function simple_slide_admin_settings_submit($form, &$form_state) {
  $style = image_style_load('simple_slide');
  foreach ($style['effects'] as $item) {
    $effects = $item;
    break;
  }
  $effects['data']['width'] = $form['simple_slide_width']['#value'];
  $effects['data']['height'] = $form['simple_slide_height']['#value'];
  image_effect_save($effects);
  image_style_flush($style);
  $num = _simple_slide_item_count();
  $slide_height = $form['simple_slide_height']['#value'];
  $slide_width = $form['simple_slide_width']['#value'];
  $caption_left = (($slide_width - ($slide_width * 0.8)) / 2 - 20) > 0 ? (($slide_width - ($slide_width * 0.8)) / 2 - 20) : 0;
  $wrap_height = $slide_height + 30;
  $wrap_width = $slide_width + 30;
  $nav_top = $slide_height / 2 - 22;
  $caption_height = $slide_height / 4;
  $caption_width = $slide_width * 0.8;
  $pagination_width = $num * 14;
  $pagination_margin1 = $slide_height / 15;
  $pagination_margin2 = ($slide_width - (14 * $num)) / 2;
  $today = date("F j, Y, g:i:s a");
  $style = <<<EOF
/**
 * @file simple_slide_custom.css
 * This file is programmatically auto create at {$today}.
 */

#simple-slide-wrap {
  height: {$wrap_height}px;
  width: {$wrap_width}px;
}
#simple-slide .pagination {
   margin: {$pagination_margin1}px {$pagination_margin2}px;
  width: {$pagination_width}px;
}
#simple-slide .caption {
  height: {$caption_height}px;
  left: {$caption_left}px;
  width: {$caption_width}px;
}
#simple-slide .pn-nav a {
  top: {$nav_top}px;
}
#simple-slide,
#simple-slide .slides_container,
#simple-slide .slide,
#simple-slide .slides_container div.slide {
  height: {$slide_height}px;
  width: {$slide_width}px;
}

EOF;
  $slide_bottom = $slide_height / 4 + $slide_height / 20;
  $script = <<<EOF
/**
 * @file simple_slide_custom.js
 * This file is programmatically auto create at {$today}.
 */

$(function(){
  $('#simple-slide').slides({
    preload: {$form['simple_slide_preload']['#value']},
    preloadImage: Drupal.settings.simple_slide.path + "/img/loading.gif",
    play: {$form['simple_slide_play']['#value']},
    hoverPause: 1,
    pause: {$form['simple_slide_pause']['#value']},
    slideEasing: "{$form['simple_slide_easing']['#value']}",
    fadeEasing: "{$form['simple_slide_easing']['#value']}",
    effect: "{$form['simple_slide_effect']['#value']}",
    fadeSpeed: {$form['simple_slide_effectSpeed']['#value']},
    slideSpeed: {$form['simple_slide_effectSpeed']['#value']},
    randomize: {$form['simple_slide_randomize']['#value']},
    playControl: {$form['simple_slide_playControl']['#value']},
    generatePagination: {$form['simple_slide_generatePagination']['#value']},

    animationStart: function(current){
      $('#simple-slide .caption').animate({
        bottom: -{$slide_bottom}
      }, 100);
    },
    animationComplete: function(current){
      $('#simple-slide .caption').animate({
        bottom: 0
      }, 200);
    },
    slidesLoaded: function() {
      $('#simple-slide .caption').animate({
        bottom: 0
      }, 200);
    }
  });
});

EOF;
  $path = drupal_get_path('module', 'simple_slide');
  file_unmanaged_save_data($style, $path . '/css/simple_slide_auto.css', FILE_EXISTS_REPLACE);
  file_unmanaged_save_data($script, $path . '/js/simple_slide_auto.js', FILE_EXISTS_REPLACE);
  return TRUE;
}
