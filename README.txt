/**
 *  Simple Slide
 *  Update: 2012/05/20
 */


Summary
-------
Simple Slide project is provide a new content type to display the fancy
slides show. You can create your slide from node adding page. To start
this project is because many of carousel project doesn't provide "content"
area for display some text in carousel.


Basic Functionality
-------------------
Here is feature list of this module:

* Show fancy slider
* Create a new content type which name is "Simple Slide"
* Configuration animation slide size, readmore botton, speed, effect and many...
* Provide a Block to use
* Custom this style sheet


Requirements and Dependencies
-----------------------------
* Field                      --- Drupal Core Module
* Image                      --- Drupal Core Module
* Link                       --- http://drupal.org/project/link


Installation and Settings
-------------------------
TO successful completion of this installation, you must to install above "Dependencies"
module, and according to the following steps:

1) Enable the module using Administration -> Modules (/admin/modules).

2) Create a new content "Simple Slide" (/node/add/simple-slide).
   Every this content will be display in the block.
   
3) Settinga and onfigure this module (/admin/config/media/simple_slide).


Author
------
  Albert Liu (dreamerhyde@gmail.com, http://drupal.org/user/1374916)

  If you use this module, find it useful, and want to send the author
  a thank or you note, feel free to contact me.

  The author can also be contacted for paid customizations of this and other modules.
