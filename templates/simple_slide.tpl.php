<?php
/**
 * @file simple_slide.tpl.php
 * Provide to Simple Slide Block.
 */
?>

<div id="simple-slide-wrap">
  <div id="simple-slide">
    <div class="slides_container">
      <?php foreach($slide_img as $k => $img): ?>
      <div class="slide">
        <?php print $img; ?>
        <div class="caption" style="bottom:0">
          <div class="header">
            <h1><?php print $title[$k]; ?></h1>
            <span class="meta"><?php print $meta[$k]; ?></span>
          </div>
          <p><?php print $content[$k]; ?></p>
        </div>
      </div>
      <?php endforeach; ?>
    </div>
    <div class="pn-nav">
      <a href="#" class="prev"><img src="<?php print $slide_path; ?>/img/arrow-prev.png" width="24" height="43" alt="Arrow Prev"></a>
      <a href="#" class="next"><img src="<?php print $slide_path; ?>/img/arrow-next.png" width="24" height="43" alt="Arrow Next"></a>
    </div>
  </div>
</div>
