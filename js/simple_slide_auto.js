/**
 * @file simple_slide_custom.js
 * This file is programmatically auto create at May 20, 2012, 11:46:44 pm.
 */

$(function(){
  $('#simple-slide').slides({
    preload: 1,
    preloadImage: Drupal.settings.simple_slide.path + "/img/loading.gif",
    play: 5000,
    hoverPause: 1,
    pause: 3500,
    slideEasing: "easeInOutExpo",
    fadeEasing: "easeInOutExpo",
    effect: "slide",
    fadeSpeed: 350,
    slideSpeed: 350,
    randomize: 0,
    playControl: 1,
    generatePagination: 1,

    animationStart: function(current){
      $('#simple-slide .caption').animate({
        bottom: -90
      }, 100);
    },
    animationComplete: function(current){
      $('#simple-slide .caption').animate({
        bottom: 0
      }, 200);
    },
    slidesLoaded: function() {
      $('#simple-slide .caption').animate({
        bottom: 0
      }, 200);
    }
  });
});
